// DO NOT EDIT. This is code generated via package:easy_localization/generate.dart

abstract class  LocaleKeys {
  static const app_name = 'app_name';
  static const alphabetical_sort = 'alphabetical_sort';
  static const reversed = 'reversed';
  static const copied = 'copied';
  static const copy = 'copy';
  static const paste = 'paste';
  static const num_elemen_not_rep = 'num_elemen_not_rep';
  static const no_elemen_copied = 'no_elemen_copied';
  static const run = 'run';

}
