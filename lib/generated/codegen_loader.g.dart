// DO NOT EDIT. This is code generated via package:easy_localization/generate.dart

// ignore_for_file: prefer_single_quotes

import 'dart:ui';

import 'package:easy_localization/easy_localization.dart' show AssetLoader;

class CodegenLoader extends AssetLoader{
  const CodegenLoader();

  @override
  Future<Map<String, dynamic>> load(String fullPath, Locale locale ) {
    return Future.value(mapLocales[locale.toString()]);
  }

  static const Map<String,dynamic> en_US = {
  "app_name": "Remove Duplicates",
  "alphabetical_sort": "Alphabetical Sort",
  "reversed": "Reversed",
  "copied": "Copied",
  "copy": "Copy",
  "paste": "Paste your text here",
  "num_elemen_not_rep": "Number of elements not repeated: ",
  "no_elemen_copied": "No elements to copied",
  "run": "Run"
};
static const Map<String,dynamic> pt_BR = {
  "app_name": "Remover duplicados",
  "alphabetical_sort": "Ordem Alfabética",
  "reversed": "Inverter",
  "copied": "Copiado",
  "copy": "Copiar",
  "paste": "Cole seu texto aqui",
  "num_elemen_not_rep": "Número de elementos não repetidos: ",
  "no_elemen_copied": "Sem elementos para copiar",
  "run": "Executar"
};
static const Map<String, Map<String,dynamic>> mapLocales = {"en_US": en_US, "pt_BR": pt_BR};
}
