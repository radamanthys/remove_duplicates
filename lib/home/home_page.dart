import 'package:clipboard/clipboard.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:oktoast/oktoast.dart';

import '../generated/locale_keys.g.dart';
import 'home_controller.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  late final HomeController controller;
  late final TextEditingController textController;

  @override
  void initState() {
    super.initState();
    controller = HomeController();
    textController = TextEditingController(text: '');
  }

  @override
  void dispose() {
    textController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(LocaleKeys.app_name).tr(),
      ),
      body: SingleChildScrollView(
        child: Center(
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Card(
                  elevation: 8,
                  child: Container(
                    padding: const EdgeInsets.all(8),
                    child: Wrap(
                      alignment: WrapAlignment.center,
                      children: [
                        SizedBox(
                          width: 250,
                          child: CheckboxListTile(
                            controlAffinity: ListTileControlAffinity.leading,
                            title:
                                const Text(LocaleKeys.alphabetical_sort).tr(),
                            value: controller.isAlphabeticalSort,
                            onChanged: (value) {
                              setState(() {
                                controller.isAlphabeticalSort = value!;
                              });
                            },
                          ),
                        ),
                        SizedBox(
                          width: 250,
                          child: CheckboxListTile(
                            controlAffinity: ListTileControlAffinity.leading,
                            title: const Text(LocaleKeys.reversed).tr(),
                            value: controller.isReversed,
                            onChanged: (value) {
                              setState(() {
                                controller.isReversed = value!;
                              });
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Card(
                  elevation: 8,
                  child: Container(
                    padding: const EdgeInsets.all(8),
                    child: TextField(
                      controller: textController,
                      maxLines: 15,
                      decoration: InputDecoration(
                        hintText: LocaleKeys.paste.tr(),
                        border: InputBorder.none,
                      ),
                    ),
                  ),
                ),
                Card(
                  elevation: 8,
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    padding: const EdgeInsets.all(16),
                    child: SelectableText(
                      '${LocaleKeys.num_elemen_not_rep.tr()} '
                      // 'Quantidade de elementos não repetidos:'
                      '${controller.length}\n\n'
                      // Resultado
                      '${controller.result}',
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
      floatingActionButton: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          FloatingActionButton(
            onPressed: () {
              if (controller.result.isNotEmpty) {
                setState(() {
                  FlutterClipboard.copy(controller.result).then((value) =>
                      showToast(LocaleKeys.copied.tr(),
                          position: ToastPosition.bottom));
                  // controller.removeSpace(textController.text);
                });
              } else {
                showToast(LocaleKeys.no_elemen_copied.tr(),
                    position: ToastPosition.bottom);
              }
            },
            tooltip: LocaleKeys.copy.tr(),
            child: const Icon(Icons.copy),
          ),
          const SizedBox(height: 16),
          FloatingActionButton(
            onPressed: () {
              setState(() {
                controller.removeDuplicates(textController.text);
              });
            },
            tooltip: LocaleKeys.run.tr(),
            child: const Icon(Icons.rule),
          ),
        ],
      ),
    );
  }
}
