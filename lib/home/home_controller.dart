class HomeController {
  String result = '';
  int length = 0;

  bool isAlphabeticalSort = false;
  bool isReversed = false;

  void removeDuplicates(String value) {
    // Limpa o resultado
    result = '';
    // Lista temporaria
    List<String> tmpList = [];

    // Remove espaço vazios e transforma em vírgula
    String replace = value.replaceAll(RegExp('\\s+'), ',');
    //  var replace = r.replaceAll('\n', ',');

    // Usa a vírgula como delimitador para criar elementos de uma lista
    List<String> split = replace.split(',');

    // Remove os elementos vazios
    split.removeWhere((e) => e == '');
    // split.removeWhere((e) => e == ' ');

    // Remove os elementos repetidos
    List<String> listSet = split.toSet().toList();

    if (isAlphabeticalSort) {
      // Coloca em ordem alfabética ignorando letras maiúsculas (opcional)
      listSet.sort((a, b) => a.toLowerCase().compareTo(b.toLowerCase()));
    }

    // Recebe a lista com os dados repitidos removidos
    tmpList = listSet;

    if (isReversed) {
      // Inverter a lista (opcional)
      List<String> reversed = listSet.reversed.toList();
      tmpList = reversed;
    }

    length = tmpList.length;
    result = tmpList
        .toString()
        .replaceAll('[', '')
        .replaceAll(']', '')
        .replaceAll(', ', '\n');
  }
}
