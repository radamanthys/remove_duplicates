import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:oktoast/oktoast.dart';
import 'package:remove_duplicate/generated/locale_keys.g.dart';
import 'package:responsive_framework/responsive_framework.dart';

import 'generated/codegen_loader.g.dart';
import 'home/home_page.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await EasyLocalization.ensureInitialized();

  runApp(
    EasyLocalization(
      supportedLocales: const [Locale('en', 'US'), Locale('pt', 'BR')],
      path: 'assets/translations',
      // fallbackLocale: const Locale('en', 'US'),
      assetLoader: const CodegenLoader(),
      child: App(),
    ),
  );
}

// ignore: use_key_in_widget_constructors
class App extends StatelessWidget {
  static const MaterialColor darkColor = MaterialColor(
    _darkPrimaryValue,
    <int, Color>{
      50: Color.fromRGBO(17, 24, 39, .1),
      100: Color.fromRGBO(17, 24, 39, .2),
      200: Color.fromRGBO(17, 24, 39, .3),
      300: Color.fromRGBO(17, 24, 39, .4),
      400: Color.fromRGBO(17, 24, 39, .5),
      500: Color(_darkPrimaryValue),
      600: Color.fromRGBO(17, 24, 39, .7),
      700: Color.fromRGBO(17, 24, 39, .8),
      800: Color.fromRGBO(17, 24, 39, .9),
      900: Color.fromRGBO(17, 24, 39, 1),
    },
  );
  static const int _darkPrimaryValue = 0xFF151e31;

  @override
  Widget build(BuildContext context) {
    return OKToast(
      child: MaterialApp(
        builder: (context, widget) => ResponsiveWrapper.builder(
          BouncingScrollWrapper.builder(context, widget!),
          maxWidth: 1200,
          minWidth: 480,
          defaultScale: true,
          breakpoints: const [
            ResponsiveBreakpoint.resize(450, name: MOBILE),
            ResponsiveBreakpoint.autoScaleDown(800, name: TABLET),
            ResponsiveBreakpoint.autoScale(1000, name: TABLET),
            ResponsiveBreakpoint.resize(1200, name: DESKTOP),
            ResponsiveBreakpoint.autoScale(2460, name: "4K"),
          ],
          background: Container(color: const Color(0xFFfffafa)),
        ),
        title: LocaleKeys.app_name.tr(),
        theme: ThemeData(
          scaffoldBackgroundColor: const Color(0xFFfffafa),
          colorScheme: ColorScheme.fromSwatch(primarySwatch: darkColor)
              .copyWith(secondary: Colors.black),
        ),
        localizationsDelegates: context.localizationDelegates,
        supportedLocales: context.supportedLocales,
        locale: context.locale,
        home: const HomePage(),
      ),
    );
  }
}
